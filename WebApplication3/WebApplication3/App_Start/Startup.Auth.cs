﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.OAuth;
using Owin;
using WebApplication3.Models;
using WebApplication3.Providers;
using CelloSaaS.Owin.Security;
using CelloSaaS.Owin.Security.GenericOAuth2Authentication;
using System.Collections.Generic;
using CelloSaaS.Owin.Security.GenericOAuth2Authentication.Provider;
using System.Security.Claims;


namespace WebApplication3
{
    public partial class Startup
    {
        // Enable the application to use OAuthAuthorization. You can then secure your Web APIs
        static Startup()
        {
            PublicClientId = "web";

            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/Token"),
                AuthorizeEndpointPath = new PathString("/account/authorize"),
                Provider = new ApplicationOAuthProvider(PublicClientId),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(14),
                AllowInsecureHttp = true
            };


        }

        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        public static string PublicClientId { get; private set; }

        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            // Configure the db context, user manager and signin manager to use a single instance per request
            app.CreatePerOwinContext(ApplicationDbContext.Create);
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);

            // Enable the application to use a cookie to store information for the signed in user
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                Provider = new CookieAuthenticationProvider
                {
                    // Enables the application to validate the security stamp when the user logs in.
                    // This is a security feature which is used when you change a password or add an external login to your account.  
                    OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<ApplicationUserManager, ApplicationUser>(
                        validateInterval: TimeSpan.FromMinutes(20),
                        regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager))
                }
            });
            // Use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Enables the application to temporarily store user information when they are verifying the second factor in the two-factor authentication process.
            //app.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));

            // Enables the application to remember the second login verification factor such as phone or email.
            // Once you check this option, your second step of verification during the login process will be remembered on the device where you logged in from.
            // This is similar to the RememberMe option when you log in.
            //app.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);

            app.UseOAuthAuthorizationServer(OAuthOptions);
            // Enable the application to use bearer tokens to authenticate users
            app.UseOAuthBearerTokens(OAuthOptions);

            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "");

            //app.UseTwitterAuthentication(
            //    consumerKey: "",
            //    consumerSecret: "");

            //app.UseFacebookAuthentication(
            //    appId: "",
            //    appSecret: "");

            var authOptions = new GenericOAuth2AuthenticationOptions("CelloAuth")
            {
                SignInAsAuthenticationType = DefaultAuthenticationTypes.ExternalCookie,
                AccessType = "CelloAuth",
                AuthenticationMode = Microsoft.Owin.Security.AuthenticationMode.Passive,
                AuthorizationEndpointUri = "http://localhost:37202/authorize",
                TokenEndpointUri = "http://localhost:37202/api/token",
                UserInfoEndpointUri = "http://localhost:37202/api/userinfo",
                Scope = new List<string> { "all" },
                Provider = new GenericOAuth2AuthenticationProvider
                {
                    InitRequestFromAuthenticationProperties = EndpointUriTypes => true,
                    OnMapContextToClaims = async context =>
                    {
                        List<Claim> additionalClaims = new List<Claim>();
                        if (!string.IsNullOrEmpty(context.Id))
                        {
                            additionalClaims.Add(new Claim(CelloClaimTypes.UserId, context.Id));
                        }
                        if (!string.IsNullOrEmpty(context.ClientId))
                        {
                            additionalClaims.Add(new Claim(CelloClaimTypes.ClientId, context.ClientId));
                        }
                        if (!string.IsNullOrEmpty(context.TenantId))
                        {
                            additionalClaims.Add(new Claim(CelloClaimTypes.TenantId, context.TenantId));
                            additionalClaims.Add(new Claim(CelloClaimTypes.LoggedInTenantId, context.TenantId));
                        }
                        if (!string.IsNullOrEmpty(context.Scope))
                        {
                            additionalClaims.Add(new Claim(CelloClaimTypes.Scopes, context.Scope));
                        }
                        if (!string.IsNullOrEmpty(context.Profile))
                        {
                            additionalClaims.Add(new Claim(CelloClaimTypes.Profile, context.Profile));
                        }

                        if (!string.IsNullOrEmpty(context.Role))
                        {
                            additionalClaims.Add(new Claim(ClaimTypes.Role, string.Join(",", context.Role)));
                            additionalClaims.Add(new Claim(CelloClaimTypes.LoggedInUserRoles, string.Join(",", context.Role)));
                        }
                        if (context.ExpiresIn != null && context.ExpiresIn.HasValue && context.ExpiresIn.Value.TotalSeconds > 0)
                        {
                            additionalClaims.Add(new Claim(ClaimTypes.Expiration, context.ExpiresIn.Value.TotalSeconds.ToString()));
                        }
                        return additionalClaims;
                    }
                }
            };

            app.Map(new PathString("/Account/ExternalLogin"), loginApp =>
            {
                loginApp.UseGenericOAuth2Authentication(authOptions);
            });
            app.Map(new PathString("/Account/Login"), loginApp =>
            {
                loginApp.UseGenericOAuth2Authentication(authOptions);
            });
        }
    }
}
