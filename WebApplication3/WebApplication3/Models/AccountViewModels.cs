﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApplication3.Models
{
    // Models returned by AccountController actions.
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Hometown")]
        public string Hometown { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Hometown")]
        public string Hometown { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public static class CelloClaimTypes
    {
        public const string ClientId = "http://schemas.microsoft.com/ws/2008/06/identity/claims/clientid";
        public const string LoggedInTenantId = "http://schemas.microsoft.com/ws/2008/06/identity/claims/loggedintenantid";
        public const string LoggedInUserRoles = "http://schemas.microsoft.com/ws/2008/06/identity/claims/loggedinuserroles";
        public const string LoggedInUserTenantId = "http://schemas.microsoft.com/ws/2008/06/identity/claims/loggedinusertenantid";
        public const string Privileges = "http://schemas.microsoft.com/ws/2008/06/identity/claims/privileges";
        public const string Profile = "http://schemas.microsoft.com/ws/2008/06/identity/claims/profile";
        public const string Scopes = "http://schemas.microsoft.com/ws/2008/06/identity/claims/scopes";
        public const string SessionRoles = "http://schemas.microsoft.com/ws/2008/06/identity/claims/sessionroles";
        public const string SessionTenantId = "http://schemas.microsoft.com/ws/2008/06/identity/claims/sessiontenantid";
        public const string TenantId = "http://schemas.microsoft.com/ws/2008/06/identity/claims/tenantid";
        public const string UserId = "http://schemas.microsoft.com/ws/2008/06/identity/claims/userid";
        
    }

    public static class CelloConstants
    {
        public const string MultiTenantClientID = "5DF78E31-1DCA-4DCD-832F-C28B599FFF5B";
    }
}
