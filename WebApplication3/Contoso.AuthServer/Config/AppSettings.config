﻿<appSettings>
  <!-- License activation key obtained after activating the product, unique to a machine -->
  <add key="ActivationKey" value=""/>

  <!-- default application culture e.g: en=english, ja=japanese, fr=french-->
  <add key="DefaultCulture" value="en"/>

  <!-- Application home page url - redirected to this page after login -->
  <add key="AppHomePageUrl" value=""/>
  <!-- Application root url - i.e.: hppt://domain.com -->
  <add key="ApplicationUrl" value=""/>

  <!-- Enable SSO (ADFS WIF) -->
  <add key="EnableFederation" value="false"/>
  <add key="DefaultHomeRealm" value=""/>

  <!-- Page Profiler toolbar -->
  <add key="PageProfiler" value="True"/>
  <!-- Only the users with this rule can view the Page Profiler toolbar -->
  <add key="PageProfiler_Role" value=""/>

  <add key="ApplicationDirectoryPath" value=""/>
  <add key="LogoImageFolderPath" value="LogoImageFolder"/>
  <add key="DefaultTheme" value="CelloSkin"/>
  <add key="DefaultLogo" value="cellosaas.svg"/>
  <add key="UseServerValidation" value="true"/>
  <add key="AccessDenied" value="****"/>
  <add key="IsCacheEnabled" value="True"/>
  <add key="CacheRefreshTime" value="1440"/>
  <add key="AutoIsolationTenant" value="false"/>
  <add key="Mapping" value="MappingFiles" />
  
  <!-- MVC settings -->
  <add key="ClientValidationEnabled" value="true"/>
  <add key="UnobtrusiveJavaScriptEnabled" value="true"/>

  <!-- If true then user password is auto generated if the contact email is present else default password is set -->
  <add key="AutoGenerateUserPassword" value="true"/>
  <!-- Default password to use if auto generate is set to false or contact email is not available -->
  <add key="DefaultUserPassword" value="company#123"/>
  <!-- To detect online users. last activity time > now - (this value in minutes) default Form Session Timeout in minutes-->
  <add key="OnlineUserActiveMinutes" value="10"/>
  <!-- Enable UserActivity Tracing. Possible Values are [true / false]-->
  <add key="EnableUserActivityTrace" value="false"/>

  <!-- Tenant Data backup file path-->
  <add key="DataBackupPath" value="D:\TenantDataBackup" />
  <!-- Enable Product analytics -->
  <add key="DisableProductAnalytics"  value="false"/>
  <!-- Tenant analitics setting -->
  <add key="HotTrialFrequency" value="High:70,100|Medium:40,70|Low:0,40"/>
  <add key="DalConnectionStringManager" value="DalConnectionStringProvider"/>
  <!-- To turn OFF or ON the Entity CURD opertions auto Business Rule execution feature-->
  <add key="EnableEntityCURDBusinessRule" value="true"/>
  <!-- To turn OFF or ON the Audit Trail Feature -->
  <add key="EnableAuditTrail" value="false"/>
  <!-- To turn OFF or ON the Extended Fields for the Entities-->
  <add key="EnableExtendedFields" value="true"/>
  <!-- Enable FlatTable or KeyValueTable -->
  <add key="ExtnTableFormat" value="FlatTable"/>
  <!-- To enable or disable the automatic table source creation for every query source-->
  <add key="AutoCreateTableSource" value="true"/>
  <add key="BusinessRuleWcfURL" value=""/>

  <!-- User password validation regex -->
  <add key="ValidationExpression" value="^((?=.*\d)(?=.*[\p{L}])(?=.*[@#$%]).{6,20})$"/>
  <!-- Supported Formats : Hashed (OR) Encrypted -->
  <add key="PasswordFormat" value="Encrypted"/>
  <add key="PasswordExpiryDays" value="0"/>
  <add key="ValidationFailureMessage" value="Password should contain atleast one digit, one alphabet, one in the [@#$%] and should be between 6 - 20 characters"/>
  <add key="ValidationSuccessMessage" value="Password Validation Success!"/>

  <!-- Provide the full path of the location of the error view page. This will be displayed for normal requests -->
  <add key="ErrorViewPagePath" value="~/Views/Error/Index.aspx"/>
  <!-- Provide the full path of the location of the error view page. This will be displayed for AJAX requests -->
  <add key="ErrorPartialViewPagePath" value="~/Views/Error/PartialIndex.ascx"/>

  <!-- Display the available date format in tenant settings template separated by ; -->
  <add key="DateTimeFormats" value="MM/dd/yyyy;dd/MM/yyyy;yyyy/MM/dd;dddd, dd MMMM yyyy;dddd, dd MMMM yyyy HH:mm;dddd, dd MMMM yyyy hh:mm tt;dddd, dd MMMM yyyy h:mm tt;dddd, dd MMMM yyyy H:mm;MM/dd/yyyy HH:mm;dddd, dd MMMM yyyy HH:mm:ss;MM/dd/yyyy H:mm;MM/dd/yyyy hh:mm tt;MMMM dd;MM/dd/yyyy HH:mm:ss;yyyy'-'MM'-'dd'T'HH':'mm':'ss.fffffffK;ddd, dd MMM yyyy HH':'mm':'ss 'GMT';yyyy'-'MM'-'dd'T'HH':'mm':'ss;HH:mm;hh:mm tt;H:mm;h:mm tt;HH:mm:ss;yyyy'-'MM'-'dd HH':'mm':'ss'Z';dddd, dd MMMM yyyy HH:mm:ss;yyyy MMMM"/>

  <!-- Cache Synchronization -->
  <add key="CacheServerEndpoints" value=""/>
  <add key="CacheSyncNames" value="CelloSaaS"/>

  <!-- Billing -->
  <add key="ApplyGlobalBillSettingForPrepaidMode" value="False"/>
  <!-- PayPal settings -->
  <add key="PAYPAL_REDIRECT_URL" value="https://www.sandbox.paypal.com/webscr&amp;cmd="/>
  <add key="Payment_CancelUrl" value="http://localhost:5000/Billing/PaymentCancelled"/>
  <add key="Payment_ReturnUrl" value="http://localhost:5000/Billing/PaymentApproved"/>
  <!-- Default invoice note appended at the bottom of every bills -->
  <add key="DefaultInvoiceNote" value="&lt;p&gt;Please find the cost-breakdown for the services used. Please make payment at your earliest convenience, and do not hesitate to contact me with any questions.&lt;/p&gt;&lt;br/&gt;&lt;p&gt;Payment terms: to be received within 30 days.&lt;/p&gt;"/>

  <!-- True for WCF mode -->
  <add key="UseAPIKey" value="False"/>
  <!-- common api key to use for all WCF calls -->
  <add key="CommonApiKey" value=""/>

  <!--To send the activation mail to newly created tenant -->
  <add key="SendTenantActivationLink" value="True"/>
  <!-- Activation link generated will exprire after this days -->
  <add key="TenantActivationExpirationDays" value="10"/>
  <!-- Activation URL path -->
  <add key="TenantActivationUrl" value="http://localhost/TenantSelfRegistration/Activate"/>

  <!-- Tenant Self Registration Controller name -->
  <add key="TenantSelfRegistrationController" value="TenantSelfRegistration"/>
  <add key="RoboUserTenantCodeString" value="company"/>
  <add key="RoboUserUserName" value="admin@company.com"/>

  <!-- Absolute path to Module/Feature configuration xml files e.g.: E:\Websites\App\ModulesConfiguration\ -->
  <add key="ModuleConfiguration" value=""/>
  <add key="log4net.Config" value="log4net.config" />
  <add key="CelloOpenIdProviderId" value="d1179b78-b86d-4b79-bd4b-615764ff15bb" />
  <add key="owin:appStartup" value="Contoso.AuthServer.Startup, Contoso.AuthServer" />
</appSettings>