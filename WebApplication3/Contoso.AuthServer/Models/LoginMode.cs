﻿namespace Contoso.AuthServer.Models
{
    public enum LoginMode : int
    {
        External,
        CelloIdp,
        ADFS
    }
}