// Copyright (c) Microsoft Open Technologies, Inc. All rights reserved. See License.txt in the project root for license information.

using System.Reflection;

[assembly: AssemblyCompany("TechCello")]
[assembly: AssemblyProduct("CelloSaaS")]
[assembly: AssemblyCopyright("\x00a9 TechCello All rights reserved.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyInformationalVersion("1.0.0.0")]
[assembly: AssemblyMetadata("Serviceable", "True")]
